package br.com.mobilesaude.servicoteste.config;

import static javax.ws.rs.core.HttpHeaders.AUTHORIZATION;

import java.io.IOException;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;

import br.com.mobilesaude.servicoteste.bean.LogAcessoBean;
import br.com.mobilesaude.servicoteste.retorno.RetornoLogsAcessosUsuario;

@Provider //Registra o filtro com JAX-RS runtime
@Priority(Priorities.AUTHORIZATION) //Priorize este filtro acima de outros como um filtro de segurança
@SecureAuth
public class LoggerApiFilter implements ContainerRequestFilter {

	public static final String BEARER = "Bearer";
	
	@Inject
	LogAcessoBean logAcesso;
	 
	@Context
	 private SecurityContext securityContext;
	 
	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		
		String authString = requestContext.getHeaderString(AUTHORIZATION);
		if (authString != null && !authString.isEmpty() && authString.startsWith(BEARER)) {
			try {
				logAcesso.logaAcesso(Long.parseLong(requestContext.getSecurityContext().getUserPrincipal().getName()), requestContext.getUriInfo().getPath());
			} catch (Exception e) {
				e.printStackTrace();
				requestContext.abortWith(Response
										.status(Response.Status.SERVICE_UNAVAILABLE)
										.entity(new RetornoLogsAcessosUsuario()
												.setError("Sistema indisponível")).build());
			}
		}
	}

	
	
}
