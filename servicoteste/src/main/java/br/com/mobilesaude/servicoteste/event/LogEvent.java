package br.com.mobilesaude.servicoteste.event;

import java.io.Serializable;

import javax.ws.rs.core.UriInfo;

public class LogEvent implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long idUsuario;
	
	private UriInfo uri;
	
	public LogEvent() {}
	
	public LogEvent(Long idUsuario, UriInfo uri) {
		this.idUsuario = idUsuario;
		this.uri = uri;
	}
	
	public Long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public UriInfo getUri() {
		return uri;
	}

	public void setUri(UriInfo uri) {
		this.uri = uri;
	}
	
	@Override
	public String toString() {
		return idUsuario + " URI: "  + uri.getPath();
	}

	
}
