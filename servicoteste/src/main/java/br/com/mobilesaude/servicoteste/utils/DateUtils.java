package br.com.mobilesaude.servicoteste.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Date;

public final class DateUtils {

	 private DateUtils() {}
	 static final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	 
	 /**
	   * Returns the {@link Date} of midnight at the start of the given {@link Date}.
	   *
	   * <p>This returns a {@link Date} formed from the given {@link Date} at the time of midnight,
	   * 00:00, at the start of this {@link Date}.
	   *
	   * @return the {@link Date} of midnight at the start of the given {@link Date}
	 * @throws ParseException 
	   */
	 public static LocalDateTime atStartOfDay(String date) throws ParseException {
	    LocalDateTime localDateTime = dateToLocalDateTime(formatter.parse(date));
	    //LocalDateTime startOfDay = localDateTime.with(LocalTime.MIN);
	    return localDateTime.with(LocalTime.MIN);
	    //return localDateTimeToDate(startOfDay);
	  }
	 
	 /**
	   * Returns the {@link Date} at the end of day of the given {@link Date}.
	   *
	   * <p>This returns a {@link Date} formed from the given {@link Date} at the time of 1 millisecond
	   * prior to midnight the next day.
	   *
	   * @return the {@link Date} at the end of day of the given {@link Date}j
	 * @throws ParseException 
	   */
	  public static LocalDateTime atEndOfDay(String date) throws ParseException {
	    LocalDateTime localDateTime = dateToLocalDateTime(formatter.parse(date));
	    return localDateTime.with(LocalTime.MAX);
	    //LocalDateTime endOfDay = localDateTime.with(LocalTime.MAX);
	    //return localDateTimeToDate(endOfDay);
	  }
	  
	  private static LocalDateTime dateToLocalDateTime(Date date) {
	    return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
	  }

	  static Date localDateTimeToDate(LocalDateTime localDateTime) {
	    return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
	  }
	 
	 
}
