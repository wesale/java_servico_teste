package br.com.mobilesaude.servicoteste.bean;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.mobilesaude.servicoteste.model.ExtratoUtilizacaoPlanoUsuarioTO;

@Stateless
public class ExtratoUtilizacaoPlanoBean {
	
	@PersistenceContext
	private EntityManager em;

	/**
	 * Retorna os dados de utlizacao do plano do usuario
	 * 
	 * @param idUsuario
	 * @param ano
	 * @param mes
	 * @return UsuarioTO
	 */
	public List<ExtratoUtilizacaoPlanoUsuarioTO> retornaExtratoUtilizacaoPlanoUsuario(Long idUsuario, Integer ano, String mes) {
		em.clear();
		return em.createNamedQuery(ExtratoUtilizacaoPlanoUsuarioTO.FIND_EXTRATO_UTILIZACAO_PLANO_BY_ID_USUARIO_ANO_MES, ExtratoUtilizacaoPlanoUsuarioTO.class)
				.setParameter("id_usuario", idUsuario)
				.setParameter("ano", ano)
				.setParameter("mes", Integer.parseInt(mes))
				.getResultList();
	}
}
