package br.com.mobilesaude.servicoteste.servicos.logacesso;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.validation.constraints.Pattern;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.mobilesaude.servicoteste.bean.LogAcessoBean;
import br.com.mobilesaude.servicoteste.config.SecureAuth;
import br.com.mobilesaude.servicoteste.model.LogsAcessoTO;
import br.com.mobilesaude.servicoteste.retorno.LogsAcessosUsuario;
import br.com.mobilesaude.servicoteste.retorno.RetornoLogsAcessosUsuario;
import br.com.mobilesaude.servicoteste.utils.DateUtils;

@Path("logs/v1")
//@RequestScoped
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class LogAcessoUsuarioService {
	
	@Inject
	private LogAcessoBean logAcessoBean;
	
	public static final String ERRO_DETALHE_LOG = "Nenhum registro encontrado.";

	@GET
	@Path("/")
	@SecureAuth
	public Response getLogs(@QueryParam("usuario") String usuario, @QueryParam("endpoint") String endpoint, 
			  @QueryParam("from") @Pattern(regexp = "^\\d{4}-\\d{2}-\\d{2}$", message="Formato de data inválido. Informe yyyy-MM-dd") String from, 
			  @QueryParam("to") @Pattern(regexp = "^\\d{4}-\\d{2}-\\d{2}$", message="Formato de data inválido. Informe yyyy-MM-dd") String to) {
		  try {
			  LocalDateTime dtIni = DateUtils.atStartOfDay(from);
			  LocalDateTime dtFim = DateUtils.atEndOfDay(to);
			  
			  RetornoLogsAcessosUsuario retornoLogsAcessoUsuarioDTO = new RetornoLogsAcessosUsuario();
			  List<LogsAcessoTO> logsAcessosUsuario = logAcessoBean.retornaLogsAcesso(usuario, endpoint, dtIni, dtFim);
			  
			  
			  if( logsAcessosUsuario == null ) {
			  
				  RetornoLogsAcessosUsuario a = new RetornoLogsAcessosUsuario();
				  a.setStatus(false);
				  a.setError(ERRO_DETALHE_LOG);
				  retornoLogsAcessoUsuarioDTO.setLogsAcessos(null);
				  return Response.status(Response.Status.OK).entity(retornoLogsAcessoUsuarioDTO).build();
			  }
			  // Preencher objetos de retorno para	montar o JSON ExtratoUtilizacaoPlanoUsuarioDTO
			  List<LogsAcessosUsuario> logs = logsAcessosUsuario.stream().map(LogsAcessosUsuario::new)
					  											.collect(Collectors.toList());
			  retornoLogsAcessoUsuarioDTO.setLogsAcessos(logs);
			  
			  return Response.ok().entity(retornoLogsAcessoUsuarioDTO).build();
		  
		  } catch (Exception e) {
			  e.printStackTrace();
			  return Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(new RetornoLogsAcessosUsuario().setError("Sistema indisponível")).build();
		  }
	  }
}
