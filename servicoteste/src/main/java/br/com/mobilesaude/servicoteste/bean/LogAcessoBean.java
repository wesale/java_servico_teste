package br.com.mobilesaude.servicoteste.bean;

import java.time.LocalDateTime;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import br.com.mobilesaude.servicoteste.event.LogEvent;
import br.com.mobilesaude.servicoteste.model.LogsAcessoTO;
import br.com.mobilesaude.servicoteste.model.UsuarioTO;

@Stateless
public class LogAcessoBean {

	@PersistenceContext
	private EntityManager em;

	public void logaAcesso(LogEvent e) {

		LogsAcessoTO logAcesso = new LogsAcessoTO();
		UsuarioTO usuario = em.getReference(UsuarioTO.class, e.getIdUsuario());
		logAcesso.setDataHoraAcesso(LocalDateTime.now());

		logAcesso.setEndpoint(e.getUri().getPath());
		logAcesso.setUsuario(usuario);
		em.persist(logAcesso);
	}
	
	public void logaAcesso(Long idUsuario, String endpoint) {

		LogsAcessoTO logAcesso = new LogsAcessoTO();
		UsuarioTO usuario = em.getReference(UsuarioTO.class, idUsuario);
		logAcesso.setDataHoraAcesso(LocalDateTime.now());

		logAcesso.setEndpoint(endpoint);
		logAcesso.setUsuario(usuario);
		em.persist(logAcesso);
	}
	
	/**
	 * Retorna os registros de acesso do usuario a API
	 * 
	 * @param idUsuario
	 * @param endpoint
	 * @param from 
	 * @param to
	 * @return Lista de logs do usuario
	 */
	public List<LogsAcessoTO> retornaLogsAcesso(String usuario, String endpoint, LocalDateTime from, LocalDateTime to) {
		em.clear();
		
		return em.createQuery("select l from LogsAcesso l join fetch l.usuario "
				+ " where l.usuario.usuario = :usuario and endpoint =:endpoint and l.dataHoraAcesso BETWEEN :from AND :to ORDER BY l.dataHoraAcesso desc", LogsAcessoTO.class)
				.setParameter("usuario", usuario)
				.setParameter("endpoint", endpoint)
				.setParameter("from", from)
				.setParameter("to", to)
				.getResultList();
		
	}


}
