package br.com.mobilesaude.servicoteste.retorno;

import com.fasterxml.jackson.annotation.JsonProperty;

import br.com.mobilesaude.servicoteste.model.ExtratoUtilizacaoPlanoUsuarioTO;

public class ExtratoUtilizacaoPlanoUsuario {

	public ExtratoUtilizacaoPlanoUsuario(ExtratoUtilizacaoPlanoUsuarioTO extrato) {
		super();
		this.codigoProcedimento = extrato.getCodigoProcedimento();
		this.nomeProcedimento = extrato.getNomeProcedimento();
		this.quantidadeAutorizada = extrato.getQuantidadeAutorizada();
		this.ano = extrato.getAno();
		this.mes = extrato.getMes();
		this.valor = extrato.getValor();
	}
	
	@JsonProperty("codigoProcedimento")
	private String codigoProcedimento;
	
	@JsonProperty("nomeProcedimento")
    private String nomeProcedimento;
    
	@JsonProperty("quantidadeAutorizada")
    private Integer quantidadeAutorizada;
    
	@JsonProperty("ano")
    private Integer ano;
    
	@JsonProperty("mes")
    private Integer mes;
    
	@JsonProperty("valor")
    private Double valor;

	public ExtratoUtilizacaoPlanoUsuario() {}
	
	
	public String getCodigoProcedimento() {
		return codigoProcedimento;
	}

	public void setCodigoProcedimento(String codigoProcedimento) {
		this.codigoProcedimento = codigoProcedimento;
	}

	public String getNomeProcedimento() {
		return nomeProcedimento;
	}

	public void setNomeProcedimento(String nomeProcedimento) {
		this.nomeProcedimento = nomeProcedimento;
	}

	public Integer getQuantidadeAutorizada() {
		return quantidadeAutorizada;
	}

	public void setQuantidadeAutorizada(Integer quantidadeAutorizada) {
		this.quantidadeAutorizada = quantidadeAutorizada;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public Integer getMes() {
		return mes;
	}

	public void setMes(Integer mes) {
		this.mes = mes;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}
	
}
