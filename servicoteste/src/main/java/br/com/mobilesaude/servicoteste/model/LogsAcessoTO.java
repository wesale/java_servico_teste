package br.com.mobilesaude.servicoteste.model;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Table(name = "logs_acesso")
@Entity(name = "LogsAcesso")
public class LogsAcessoTO {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_usuario")
    @JsonIgnore
    private UsuarioTO usuario;
	
	 @Column(name = "endpoint")
	 private String endpoint; 
	 
	@Column(name = "data_hora_acesso", columnDefinition = "TIMESTAMP")
    private LocalDateTime dataHoraAcesso;

	public Long getId() {
		return id;
	}

	public UsuarioTO getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioTO usuario) {
		this.usuario = usuario;
	}

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	public LocalDateTime getDataHoraAcesso() {
		return dataHoraAcesso;
	}

	public void setDataHoraAcesso(LocalDateTime dataHoraAcesso) {
		this.dataHoraAcesso = dataHoraAcesso;
	}
	
	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}
	
	@Override
	public int hashCode() {
		return super.hashCode();
	}
}
