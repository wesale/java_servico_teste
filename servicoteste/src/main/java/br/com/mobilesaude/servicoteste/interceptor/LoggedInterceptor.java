package br.com.mobilesaude.servicoteste.interceptor;

import java.io.Serializable;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import br.com.mobilesaude.servicoteste.bean.LogAcessoBean;
import br.com.mobilesaude.servicoteste.event.LogEvent;
import br.com.mobilesaude.servicoteste.retorno.RetornoLogsAcessosUsuario;

@Interceptor
@Logged
public class LoggedInterceptor implements Serializable {

	private static final long serialVersionUID = -2230122751970857993L;
	
	public LoggedInterceptor() {}
	
	@Context
	UriInfo uriInfo;
	
	@Inject
	LogAcessoBean logAcesso;

	@AroundInvoke
    public Object logRequestEntry(InvocationContext invocationContext) throws Exception {
		
		try {
			 Object[] as = invocationContext.getParameters();
			if(as[0] instanceof LogEvent) {
				LogEvent le = (LogEvent)as[0];
				logAcesso.logaAcesso(le);
			}
			return invocationContext.proceed();
		} catch (Exception e) {
			return Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(new RetornoLogsAcessosUsuario().setError("Sistema indisponível")).build();
		}
       
    }

}
