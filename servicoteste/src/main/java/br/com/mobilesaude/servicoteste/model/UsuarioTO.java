package br.com.mobilesaude.servicoteste.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Table(name = "usuario")
@Entity(name = "Usuario")
@NamedQuery(name = UsuarioTO.FIND_USER_BY_USER, query = "select u from Usuario u where u.usuario = :usuario")
public class UsuarioTO {
	
	public static final String FIND_USER_BY_USER = "TodoUser.findByUsuario";
	
	private String salt;
	
	@Id
    @Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
    @Column(name = "usuario")
    private String usuario;
    
    @NotNull(message = "Informe uma senha válida.")
    @Size(min = 5, message = "Senha deve ter no mínimo 3 caracteres")
    @Column(name = "senha")
    private String senha;
    

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
	
	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}
	
}
