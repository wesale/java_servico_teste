package br.com.mobilesaude.servicoteste.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Table(name = "dados_usuario")
@Entity(name = "DadosUsuario")
@NamedQuery(name = DadosUsuarioTO.FIND_DADOS_USUARIOS_BY_ID_USUARIO, query = "select dU from DadosUsuario dU join fetch dU.usuario where dU.usuario.id = :id_usuario")
public class DadosUsuarioTO {
	
	public static final String FIND_DADOS_USUARIOS_BY_ID_USUARIO = "DadosUsuarioTO.findByIdUsuario";
	
    @Id
    private Long id;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_usuario")
    private UsuarioTO usuario;
	
    @Column(name = "nome")
	private String nome;
    
    @Column(name = "sobrenome")
	private String sobreNome;
    
    @Column(name = "data_nascimento")
    @Temporal(TemporalType.DATE)
    private Date dataNascimento;
	
    @Column(name = "telefone")
    private String telefone;
    
    @Column(name = "email")
    private String email;

    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UsuarioTO getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioTO usuario) {
		this.usuario = usuario;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSobreNome() {
		return sobreNome;
	}

	public void setSobreNome(String sobreNome) {
		this.sobreNome = sobreNome;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
    
}
