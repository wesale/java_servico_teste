package br.com.mobilesaude.servicoteste.servicos.usuario;

import java.security.Key;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.SecurityContext;

import br.com.mobilesaude.servicoteste.bean.ExtratoUtilizacaoPlanoBean;
import br.com.mobilesaude.servicoteste.bean.Log;
import br.com.mobilesaude.servicoteste.bean.UsuarioBean;
import br.com.mobilesaude.servicoteste.config.SecureAuth;
import br.com.mobilesaude.servicoteste.config.SecurityFilter;
import br.com.mobilesaude.servicoteste.event.LogEvent;
import br.com.mobilesaude.servicoteste.model.DadosUsuarioTO;
import br.com.mobilesaude.servicoteste.model.ExtratoUtilizacaoPlanoUsuarioTO;
import br.com.mobilesaude.servicoteste.model.ParamUsuario;
import br.com.mobilesaude.servicoteste.model.UsuarioTO;
import br.com.mobilesaude.servicoteste.retorno.DadosUsuario;
import br.com.mobilesaude.servicoteste.retorno.ExtratoUtilizacaoPlanoUsuario;
import br.com.mobilesaude.servicoteste.retorno.RetornoDadosUsuario;
import br.com.mobilesaude.servicoteste.retorno.RetornoExtratoUtilizacaoPlanoUsuario;
import br.com.mobilesaude.servicoteste.retorno.RetornoUsuario;
import br.com.mobilesaude.servicoteste.retorno.Usuario;
import br.com.mobilesaude.servicoteste.servicos.security.SecurityUtil;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import javax.ws.rs.core.MediaType;
import static javax.ws.rs.core.HttpHeaders.AUTHORIZATION;

@Path("usuario/v1")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UsuarioService {

	@Inject
	private UsuarioBean usuarioBean;
	
	@Inject
	private ExtratoUtilizacaoPlanoBean extratoUtilizaoPlanoBean;

	@Inject
    @Log
    Event<LogEvent> logEvent;
	
	@Context
	private SecurityContext securityContext;
	
	@Context
	UriInfo uriInfo;
	
	@Inject
	private SecurityUtil securityUtil;
	
	public static final String ERRO_LOGIN = "Usuário não encontrado com as informações repassadas.";
	public static final String ERRO_DETALHE_USUARIO = "Usuário não encontrado com as informações repassadas.";
	public static final String ERRO_DETALHE_EXTRATO = "Dados não encontrados para os parametros informados.";
	
	@Path("create")
	@POST
    public Response criarUsuario(@NotNull @Valid UsuarioTO usuario) {
		try {
			usuarioBean.salvarUsuario(usuario);
		
			logEvent.fire(new LogEvent(usuario.getId(), uriInfo));
			
	        return Response.status(Response.Status.CREATED).entity(usuario).build();
	        
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(new RetornoUsuario().setError("Sistema indisponível")).build();
		}
    }
	
	@POST
	@Path("/retornaUsuario")
	public Response getRetornaUsuario(ParamUsuario paramUsuario) {
		try {
				if (paramUsuario == null || paramUsuario.getUsuario().isEmpty() || paramUsuario.getSenha().isEmpty()) {
					return Response.status(Response.Status.BAD_REQUEST).entity(new RetornoUsuario().setError("Usuário ou senha inválidos.")).build();
				}
				
				if (!securityUtil.authenticateUser(paramUsuario)) {
					return Response.status(Response.Status.BAD_REQUEST).entity(new RetornoUsuario().setError("Usuário ou senha inválidos.")).build();
		        }
				
				RetornoUsuario retornoUsuario = new RetornoUsuario();
				UsuarioTO usuarioTO = usuarioBean.retornaUsuario(paramUsuario);
								
				if (usuarioTO == null) {
					retornoUsuario.setStatus(false);
					retornoUsuario.setError(ERRO_LOGIN);
					retornoUsuario.setUsuario(null);
					return Response.status(Response.Status.BAD_REQUEST).entity(retornoUsuario).build();
				}
				// Preencher objetos de retorno para montar o JSON
				Usuario usuarioRetorno = new Usuario();
				usuarioRetorno.setId(usuarioTO.getId().toString());
				usuarioRetorno.setUsuario(usuarioTO.getUsuario());
				retornoUsuario.setStatus(true);
				retornoUsuario.setUsuario(usuarioRetorno);
				
				String token = getToken(usuarioTO.getId().toString());
				
				logEvent.fire(new LogEvent(usuarioTO.getId(), uriInfo));
				
				return Response.ok().header(AUTHORIZATION, SecurityFilter.BEARER + " " + token).entity(retornoUsuario).build();
			 
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(new RetornoUsuario().setError("Sistema indisponível")).build();
		}
	}
	
	@GET
	@Path("/retornaDadosDoUsuario")
	@SecureAuth
	public Response getRetornaDadosDoUsuario() {
		try {

			RetornoDadosUsuario retornoDadosUsuario = new RetornoDadosUsuario();
			
			DadosUsuarioTO dadosUsuarioTO = usuarioBean.retornaDadosDoUsuario(Long.parseLong(securityContext.getUserPrincipal().getName()));
			if (dadosUsuarioTO == null) {
				retornoDadosUsuario.setStatus(false);
				retornoDadosUsuario.setError(ERRO_DETALHE_USUARIO);
				retornoDadosUsuario.setUsuario(null);
				retornoDadosUsuario.setDadosUsuario(null);
				return Response.status(Response.Status.BAD_REQUEST).entity(retornoDadosUsuario).build();
			}

			// Preencher objetos de retorno para montar o JSON
			DadosUsuario dadosUsuarioRetorno = new DadosUsuario();
			
			Usuario usuarioRetorno = new Usuario();
			
			UsuarioTO usuario = dadosUsuarioTO.getUsuario();
			usuarioRetorno.setId(usuario.getId().toString());
			usuarioRetorno.setUsuario(usuario.getUsuario());
			
			dadosUsuarioRetorno.setIdUsuario(usuario.getId().toString());
			dadosUsuarioRetorno.setNome(dadosUsuarioTO.getNome());
			dadosUsuarioRetorno.setSobrenome(dadosUsuarioTO.getSobreNome());
			
			SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
			String dataNascimento = fmt.format(dadosUsuarioTO.getDataNascimento());
			dadosUsuarioRetorno.setDataNascimento(dataNascimento);
			
			dadosUsuarioRetorno.setTelefone(dadosUsuarioTO.getTelefone());
			dadosUsuarioRetorno.setEmail(dadosUsuarioTO.getEmail());
			
			retornoDadosUsuario.setStatus(true);
			retornoDadosUsuario.setUsuario(usuarioRetorno);
			retornoDadosUsuario.setDadosUsuario(dadosUsuarioRetorno);
			
			return Response.ok().entity(retornoDadosUsuario).build();
			
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(new RetornoDadosUsuario().setError("Sistema indisponível")).build();
		}
	 }
	
	 private String getToken(String id) {

        Key key = securityUtil.generateKey();

        String token = Jwts.builder()
        		.setSubject(id)
        		.setIssuer(uriInfo.getAbsolutePath().toString())
                .setIssuedAt(new Date())
                .setExpiration(securityUtil.toDate(LocalDateTime.now().plusMinutes(15)))
                .signWith(SignatureAlgorithm.HS512, key)
                .setAudience(uriInfo.getBaseUri().toString())
                .compact();

        return token;
     }
     
     @GET
	 @Path("/extratos")
	 @SecureAuth
	 public Response getExtratos(@Valid @NotNull @QueryParam("ano") Integer ano, 
			  @QueryParam("mes") @NotEmpty @Pattern(regexp = "^([1-9]|1[012])$", message = "O mês informado deve estar entre 1 e 12.") String mes) {
		 
		  try {
			  RetornoExtratoUtilizacaoPlanoUsuario retornoExtratoUtilizacaoPlanoUsuarioDTO = new RetornoExtratoUtilizacaoPlanoUsuario();
			  List<ExtratoUtilizacaoPlanoUsuarioTO> extratoUtilizacaoPlanoUsuario = extratoUtilizaoPlanoBean.retornaExtratoUtilizacaoPlanoUsuario(
						  																				Long.parseLong(securityContext.getUserPrincipal().getName()),
						  																				ano, mes);
			   
			  if( extratoUtilizacaoPlanoUsuario == null ) {
			  
				  RetornoExtratoUtilizacaoPlanoUsuario a = new RetornoExtratoUtilizacaoPlanoUsuario();
				  a.setStatus(false);
				  a.setError(ERRO_DETALHE_EXTRATO);
				  retornoExtratoUtilizacaoPlanoUsuarioDTO.setExtratoUtilizacao(null);
				  return Response.status(Response.Status.BAD_REQUEST).entity(retornoExtratoUtilizacaoPlanoUsuarioDTO).build();
			  }
			  // Preencher objetos de retorno para	montar o JSON ExtratoUtilizacaoPlanoUsuarioDTO
			  List<ExtratoUtilizacaoPlanoUsuario> extratos = extratoUtilizacaoPlanoUsuario.stream().map(ExtratoUtilizacaoPlanoUsuario::new)
					  											.collect(Collectors.toList());
			  
			  retornoExtratoUtilizacaoPlanoUsuarioDTO.setExtratoUtilizacao(extratos);
			  
			  return Response.ok().entity(retornoExtratoUtilizacaoPlanoUsuarioDTO).build();
		  
		  } catch (Exception e) {
			  e.printStackTrace();
			  return Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(new RetornoExtratoUtilizacaoPlanoUsuario().setError("Sistema indisponível")).build();
		  }
	  }
	  
}
