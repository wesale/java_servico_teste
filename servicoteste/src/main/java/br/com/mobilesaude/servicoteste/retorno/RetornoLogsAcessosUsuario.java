package br.com.mobilesaude.servicoteste.retorno;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RetornoLogsAcessosUsuario extends RetornoPadrao {

	@JsonProperty("logsAcessos")
	List<LogsAcessosUsuario> logsAcessos;

	public List<LogsAcessosUsuario> getLogsAcessos() {
		return logsAcessos;
	}

	public void setLogsAcessos(List<LogsAcessosUsuario> logsAcessos) {
		this.logsAcessos = logsAcessos;
	}
	
}
