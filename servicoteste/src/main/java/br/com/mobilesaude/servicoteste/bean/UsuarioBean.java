package br.com.mobilesaude.servicoteste.bean;

import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;

import br.com.mobilesaude.servicoteste.model.DadosUsuarioTO;
import br.com.mobilesaude.servicoteste.model.ParamUsuario;
import br.com.mobilesaude.servicoteste.model.UsuarioTO;
import br.com.mobilesaude.servicoteste.servicos.security.SecurityUtil;


@Stateless
public class UsuarioBean {

	@PersistenceContext
	private EntityManager em;
	
	 @Inject
	 private SecurityUtil securityUtil;
	 
	 @Context
	 private SecurityContext securityContext;
	 
	 /**
	 * Passa o usuario e senha para retornar o id do usuario a ser repassado no
	 * proximo endpoint que retorna os detalhes
	 * 
	 * @param paramUsuario
	 * @return UsuarioTO
	 */
	public UsuarioTO retornaUsuario(ParamUsuario paramUsuario) {
		
		try {
			em.clear();
			return (UsuarioTO) em.createNamedQuery(UsuarioTO.FIND_USER_BY_USER, UsuarioTO.class)
					.setParameter("usuario", paramUsuario.getUsuario())
					.getSingleResult();
		} catch (NonUniqueResultException | NoResultException e) {
			return null;
		}

	}


	/**
	 * Retorna os detalhes do usuario
	 * 
	 * @param idUsuario
	 * @return UsuarioTO
	 */
	public DadosUsuarioTO retornaDadosDoUsuario(Long idUsuario) {
		em.clear();
		try {
			return (DadosUsuarioTO) em.createNamedQuery(DadosUsuarioTO.FIND_DADOS_USUARIOS_BY_ID_USUARIO, DadosUsuarioTO.class)
					.setParameter("id_usuario", idUsuario).getSingleResult();
		} catch (NonUniqueResultException | NoResultException e) {
			return null;
		}
	}

	
    public boolean authenticateUser(ParamUsuario paramUsuario) {

        UsuarioTO usuario = retornaUsuario(paramUsuario);

        if (usuario != null) {
            return securityUtil.passwordsMatch(usuario.getSenha(), usuario.getSalt(), paramUsuario.getSenha());
        }
        return false;
    }
    
    public UsuarioTO salvarUsuario(UsuarioTO usuario) {
    	
        Integer count = countUsuario(usuario.getUsuario());

        Map<String, String> credentialMap = securityUtil.hashPassword(usuario.getSenha());

        if (usuario.getId() == null && count == 0) {
        	usuario.setSenha(credentialMap.get("hashedPassword"));
        	usuario.setSalt(credentialMap.get("salt"));
            em.persist(usuario);
        }
        credentialMap.clear();

        return usuario;
    }
    
    public Integer countUsuario(String usuario) {
    	Query query = em.createNativeQuery("select count(id) from Usuario where exists (select id from Usuario where usuario =:usuario)");
    	query.setParameter("usuario", usuario);
    	return ((Number) query.getSingleResult()).intValue();
    }
}

