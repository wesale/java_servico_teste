package br.com.mobilesaude.servicoteste.config;

import java.io.IOException;
import java.security.Key;
import java.security.Principal;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;
import javax.ws.rs.Priorities;

import br.com.mobilesaude.servicoteste.servicos.security.SecurityUtil;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;

import static javax.ws.rs.core.HttpHeaders.AUTHORIZATION;

@Provider //Registra o filtro com JAX-RS runtime
@SecureAuth //Vincula annotação SecureAuth  com este Filtro
@Priority(Priorities.AUTHENTICATION) //Priorize este filtro acima de outros como um filtro de segurança
public class SecurityFilter implements ContainerRequestFilter {

	@Inject
	private SecurityUtil securityUtil;
		
	public static final String BEARER = "Bearer";
	
	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		/*
		 * Pegue o token do cabeçalho da solicitação usando a constante AUTHORIZATION
		 * Lança uma exceção com uma mensagem se não houver token Analisa o token Se a
		 * análise for bem-sucedida, prossiga Caso contrário, lançamos uma exceção com
		 * mensagem
		 */
		String authString = requestContext.getHeaderString(AUTHORIZATION);
		
		if (authString == null || authString.isEmpty() || !authString.startsWith(BEARER)) {

            JsonObject jsonObject = Json.createObjectBuilder().add("error-message", "Nenhum Token valido foi encontrada.").build();

            throw new NotAuthorizedException(Response.status(Response.Status.UNAUTHORIZED).entity(jsonObject).build());
        }
		
		String token = authString.substring(BEARER.length()).trim();
		
		// Precisamos instanciar um novo objeto SecurityContext
		// Precisamos passar o novo objeto SecurityContext para o RequestContext
		
		 try {
			 
			 Key key = securityUtil.generateKey();
			 
			 Jws<Claims> claimsJws = Jwts.parser().setSigningKey(key).parseClaimsJws(token);
			 
	         SecurityContext originalSecurityContext = requestContext.getSecurityContext();
	         
	         SecurityContext newSecurityContext = new SecurityContext() {

	                @Override
	                public Principal getUserPrincipal() {
	                    return new Principal() {
	                        @Override
	                        public String getName() {
	                            return claimsJws.getBody().getSubject();

	                        }
	                    };
	                }

	                @Override
	                public boolean isUserInRole(String role) {
	                    return originalSecurityContext.isUserInRole(role);
	                }

	                @Override
	                public boolean isSecure() {
	                    return originalSecurityContext.isSecure();
	                }

	                @Override
	                public String getAuthenticationScheme() {
	                    return originalSecurityContext.getAuthenticationScheme();
	                }
	            };
	            
	            requestContext.setSecurityContext(newSecurityContext);
			 
		 } catch (Exception e) {
			 JsonObject jsonObject = Json.createObjectBuilder().add("error-message", "Acesso não autorizado.").build();
			 requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).entity(jsonObject).build());
		 }
		
	}

}
