package br.com.mobilesaude.servicoteste.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Table(name = "extrato_utilizacao")
@Entity(name="ExtratoUtilizacaoPlanoUsuario")
@NamedQuery(name = ExtratoUtilizacaoPlanoUsuarioTO.FIND_EXTRATO_UTILIZACAO_PLANO_BY_ID_USUARIO_ANO_MES, query = "select eUP from ExtratoUtilizacaoPlanoUsuario eUP inner join fetch eUP.usuario where eUP.usuario.id = :id_usuario and eUP.ano = :ano and eUP.mes = :mes ORDER BY eUP.id desc")
public class ExtratoUtilizacaoPlanoUsuarioTO {
	
	public static final String FIND_EXTRATO_UTILIZACAO_PLANO_BY_ID_USUARIO_ANO_MES = "ExtratoUtilizacaoPlanoUsuarioTO.findByIdUsuarioAndAnoMes";
	
	@Id
    private Long id;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_usuario")
    @JsonIgnore
    private UsuarioTO usuario;
    
    @Column(name = "codigo_procedimento")
    private String codigoProcedimento;
    
    @Column(name = "nome_procedimento")
    private String nomeProcedimento;
    
    @Column(name = "quantidade_autorizada")
    private Integer quantidadeAutorizada;
    
    private Integer ano;
    
    private Integer mes;
    
    private Double valor;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UsuarioTO getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioTO usuario) {
		this.usuario = usuario;
	}

	public String getCodigoProcedimento() {
		return codigoProcedimento;
	}

	public void setCodigoProcedimento(String codigoProcedimento) {
		this.codigoProcedimento = codigoProcedimento;
	}

	public String getNomeProcedimento() {
		return nomeProcedimento;
	}

	public void setNomeProcedimento(String nomeProcedimento) {
		this.nomeProcedimento = nomeProcedimento;
	}

	public Integer getQuantidadeAutorizada() {
		return quantidadeAutorizada;
	}

	public void setQuantidadeAutorizada(Integer quantidadeAutorizada) {
		this.quantidadeAutorizada = quantidadeAutorizada;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public Integer getMes() {
		return mes;
	}

	public void setMes(Integer mes) {
		this.mes = mes;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}    
	
}
