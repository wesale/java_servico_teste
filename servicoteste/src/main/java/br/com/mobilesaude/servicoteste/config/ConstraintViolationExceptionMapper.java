package br.com.mobilesaude.servicoteste.config;

import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class ConstraintViolationExceptionMapper implements ExceptionMapper<ConstraintViolationException> {

	@Override
	public Response toResponse(ConstraintViolationException exception) {
		
		JsonObjectBuilder errorBuilder = Json.createObjectBuilder().add("Error", "Existem erros nos dados enviados.");
        JsonObjectBuilder objectBuilder = Json.createObjectBuilder();
        
        for (ConstraintViolation<?> constraintViolation : exception.getConstraintViolations()) {
        	if(constraintViolation.getPropertyPath().toString().split("\\.").length == 2) {
                String message = constraintViolation.getMessage();
                objectBuilder.add("Causa", message);
        	} else {
        		String message = constraintViolation.getMessage();
                objectBuilder.add("Causa", message);
        	}
        }

        errorBuilder.add("CamposInvalidos", objectBuilder.build());

        return Response.status(Response.Status.BAD_REQUEST).entity(errorBuilder.build()).build();
        
	}
	
}
