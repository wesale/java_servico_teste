package br.com.mobilesaude.servicoteste.retorno;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.annotation.JsonProperty;
import br.com.mobilesaude.servicoteste.model.LogsAcessoTO;

public class LogsAcessosUsuario {

	
	public LogsAcessosUsuario(LogsAcessoTO logs) {
		this.endpoint = logs.getEndpoint();
		this.dataHoraAcesso =  logs.getDataHoraAcesso();
	}
	
	@JsonProperty("endpoint")
	private String endpoint;
	
	@JsonProperty("dataHoraAcesso")
	private LocalDateTime dataHoraAcesso;

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}
	public String getDataHoraAcesso() {
		return dataHoraAcesso.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"));
	}

	public void setDataHoraAcesso(LocalDateTime dataHoraAcesso) {
		this.dataHoraAcesso = dataHoraAcesso;
	}
	
}
