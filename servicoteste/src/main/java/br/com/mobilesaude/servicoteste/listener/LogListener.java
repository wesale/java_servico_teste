package br.com.mobilesaude.servicoteste.listener;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.enterprise.event.Observes;
import br.com.mobilesaude.servicoteste.bean.Log;
import br.com.mobilesaude.servicoteste.event.LogEvent;
import br.com.mobilesaude.servicoteste.interceptor.Logged;

@Logged
public class LogListener implements Serializable {

	private static final Logger logger = Logger.getLogger(LogListener.class.getCanonicalName());
	 
	private static final long serialVersionUID = 1L;
	
	public void login(@Observes @Log LogEvent event) {
		logger.log(Level.INFO, "LogListener - {0}", event.toString());
    }
}
