package br.com.mobilesaude.servicoteste.config;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyReader;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;

import br.com.mobilesaude.servicoteste.model.UsuarioTO;

@Provider //Registra with JAX-RS
@Produces(MediaType.APPLICATION_JSON) //Produces json rep
public class UsuarioBodyWriter implements MessageBodyWriter<UsuarioTO>, MessageBodyReader<UsuarioTO>  {

	@Override
    public boolean isWriteable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return UsuarioTO.class.isAssignableFrom(type);
    }

    @Override
    public void writeTo(UsuarioTO usuario, Class<?> type, Type genericType, Annotation[] annotations,
                        MediaType mediaType, MultivaluedMap<String, Object> httpHeaders,
                        OutputStream entityStream) throws IOException, WebApplicationException {


        JsonObject todoUserJson = Json.createObjectBuilder().add("usuario", usuario.getUsuario())
               .add("id", usuario.getId()).build();

        OutputStreamWriter streamWriter = new OutputStreamWriter(entityStream);
        streamWriter.write(todoUserJson.toString());
        streamWriter.close();
        entityStream.close();
    }

    @Override
    public boolean isReadable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return false;
    }

    @Override
    public UsuarioTO readFrom(Class<UsuarioTO> type, Type genericType, Annotation[] annotations, MediaType mediaType, MultivaluedMap<String, String> httpHeaders, InputStream entityStream) throws IOException, WebApplicationException {
        return null;
    }
}
