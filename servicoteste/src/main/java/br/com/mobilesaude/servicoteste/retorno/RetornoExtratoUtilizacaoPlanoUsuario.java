package br.com.mobilesaude.servicoteste.retorno;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RetornoExtratoUtilizacaoPlanoUsuario extends RetornoPadrao {
	
	@JsonProperty("extratoUtilizacao")
	List<ExtratoUtilizacaoPlanoUsuario> extratoUtilizacao;
	

	public List<ExtratoUtilizacaoPlanoUsuario> getExtratoUtilizacao() {
		return extratoUtilizacao;
	}

	public void setExtratoUtilizacao(List<ExtratoUtilizacaoPlanoUsuario> extratoUtilizacao) {
		this.extratoUtilizacao = extratoUtilizacao;
	}
	
}
